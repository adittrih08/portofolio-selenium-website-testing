package demoAppOnlineTest;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


import static org.junit.jupiter.api.Assertions.assertTrue;

public class loginTest {
    public WebDriver driver;
    public WebElement loginPage;
    public WebElement inputEmail;
    public WebElement inputPassword;
    public WebElement buttonLogin;
    public WebElement logout;
    public WebElement iconProfile;

    @BeforeEach
    public void setUp(){
        driver = new ChromeDriver();
        driver.get("https://demo-app.online");
        driver.manage().window().maximize();
//        loginPage = driver.findElement(By.xpath("//*[@id=\"navbar-collapse-1\"]/ul/li[7]/a"));
//        buttonLogin = driver.findElement(By.id("buttonLoginTrack"));
//        inputEmail = driver.findElement(By.id("email"));
//        inputPassword = driver.findElement(By.id("password"));
//        logout = driver.findElement(By.xpath("//*[@id=\"navbar-collapse-1\"]/ul/li[7]/ul/li[4]/a"));
//        iconProfile = driver.findElement(By.xpath("//*[@id=\"navbar-collapse-1\"]/ul/li[7]/a/i"));
    }
//
    @AfterEach
    public void afterEach(){
        driver.quit();
    }

    @Test
    public void openLoginPage() {

        loginPage = driver.findElement(By.xpath("//*[@id=\"navbar-collapse-1\"]/ul/li[7]/a"));
        loginPage.click();
        buttonLogin = driver.findElement(By.id("buttonLoginTrack"));
        assertTrue(buttonLogin.isDisplayed()); //validasi masuk login page

    }

    @Test
    public void loginWithValidAccount(){
        WebElement loginPage = driver.findElement(By.xpath("//*[@id=\"navbar-collapse-1\"]/ul/li[7]/a"));
        loginPage.click();

        inputEmail = driver.findElement(By.id("email"));
        inputPassword = driver.findElement(By.id("password"));
        buttonLogin = driver.findElement(By.id("buttonLoginTrack"));
        inputEmail.sendKeys("testing.adit01@gmail.com");
        inputPassword.sendKeys("testing321");
        buttonLogin.click();

        iconProfile = driver.findElement(By.xpath("//*[@id=\"navbar-collapse-1\"]/ul/li[7]/a/i"));
        iconProfile.click();
        logout = driver.findElement(By.xpath("//*[@id=\"navbar-collapse-1\"]/ul/li[7]/ul/li[4]/a"));
        try{
            Thread.sleep(1000);
        }
        catch(InterruptedException ie){
        }
        assertTrue(logout.isDisplayed());
    }
}
